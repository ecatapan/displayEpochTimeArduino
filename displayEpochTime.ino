
//for wifi

#include <ESP8266WiFi.h>

#include <NTPClient.h>
#include <WiFiUdp.h>

//for time
#include <Wire.h>
#include <RtcUtility.h>
#include <RtcDS3231.h>
#include <RtcDateTime.h>
#include <TimeLib.h>

//library to display to lcd
#include <LiquidCrystal_I2C.h>


// Replace with your network credentials
const char *ssid     = "Wifi_Er";
const char *password = "wmdcwifier";

int hostId = 192;
int success;
int httpResponseCode;
const char* reason;
String response;
const int GMT_8 = 28800;


WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, GMT_8);
RtcDS3231<TwoWire> Rtc(Wire);
#define countof(a) (sizeof(a) / sizeof(a[0]))
LiquidCrystal_I2C lcd(0x27,16,2);






//void setup excute only ones
void setup(){   
  
  // Initialize Serial Monitor
  Serial.begin(115200);
   
  // Connect to Wi-Fi
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  IPAddress ip(192,168,1,hostId);
  IPAddress gateway(192,168,1,100);
  IPAddress subnet(255,255,255,0);
  IPAddress dns(192,168,1,100);
  WiFi.config(ip, gateway, subnet,dns);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.print(".");
  }
 
// Initialize a NTPClient to get time
  timeClient.begin();
  // Set offset time in seconds to adjust for your timezone, for example:
  // GMT +1 = 3600
   //GMT +8 = 28800
  // GMT -1 = -3600
  // GMT 0 = 0

  lcd.init();
  lcd.backlight();
  
  Rtc.Begin();

  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);

  if (!Rtc.IsDateTimeValid()) {
    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning()) {
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();

  if (now < compiled) {
    Rtc.SetDateTime(compiled);
  } else if (now > compiled) {} else if (now == compiled) {}

  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);

}
 
void loop() {
  timeClient.update();

//  httpServer.handleClient();
  String formattedTime = timeClient.getFormattedTime();
  lcd.setCursor(0,0);
  lcd.print("Now= ");
  lcd.print(formattedTime);
  //delay(1000);

  
}
